const http = require("http");

const port = 4000;

const server = http.createServer((req, res) => {

    if (req.url == "/greeting") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        })
        res.end("Hello World")
    } else if (req.url == '/homepage') {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        })
        res.end("Welcome to the Homepage!")
    } else {
        res.writeHead(404, {
            'Content-Type': 'text/plain'
        })
        res.end("Page not found")
    }

})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`);